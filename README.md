# [symfony/mime](https://packagist.org/packages/symfony/mime)

Allows manipulating MIME messages

* Symfony5: The Fast Track [*Step 14: Accepting Feedback with Forms*](https://symfony.com/doc/current/the-fast-track/en/14-form.html)
